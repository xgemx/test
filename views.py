from app import app
from flask import request, jsonify
from models import Pupil, Grade, Subject, db
from schemas import *



# endpoint to create new pupil
@app.route("/pupil", methods=["POST"])
def add_pupil():
    name = request.json['name']

    new_pupil = Pupil()
    new_pupil.name = name

    db.session.add(new_pupil)
    db.session.commit()

    return pupil_schema.jsonify(new_pupil)


# endpoint to create new grade
@app.route("/grade", methods=["POST"])
def add_grade():

    new_grade = Grade()

    new_grade.value = request.json['value']
    owner = Pupil.query.get(request.json['owner_id'])
    new_grade.owner = owner
    subject = Subject.query.get(request.json['subject_id'])
    new_grade.subject = subject

    db.session.add(new_grade)
    db.session.commit()
    return grade_schema.jsonify(new_grade)


# endpoint to create new subject
@app.route("/subject", methods=["POST"])
def add_subject():
    name = request.json['name']

    new_subject = Subject()

    new_subject.name = name

    db.session.add(new_subject)
    db.session.commit()

    return subject_schema.jsonify(new_subject)

# endpoint to show all grades
@app.route("/grade", methods=["GET"])
def get_grades():
    all_grades = Grade.query.all()
    result = grades_schema.dump(all_grades)
    return jsonify(result.data)

# endpoint to show all subjects
@app.route("/subject", methods=["GET"])
def get_subjects():
    all_subjects = Subject.query.all()
    result = subjects_schema.dump(all_subjects)
    return jsonify(result.data)

# endpoint to show all pupils
@app.route("/pupil", methods=["GET"])
def get_pupils():
    all_pupils = Pupil.query.all()
    result = pupils_schema.dump(all_pupils)
    return jsonify(result.data)


# endpoint to update pupil
@app.route("/pupil/<id>", methods=["PUT"])
def pupil_update(id):
    pupil = Pupil.query.get(id)
    name = request.json['name']

    pupil.name = name

    db.session.commit()
    return pupil_schema.jsonify(pupil)

# endpoint to update grade
@app.route("/grade/<id>", methods=["PUT"])
def grade_update(id):
    grade = Grade.query.get(id)
    value = request.json['value']
    owner = Pupil.query.get(request.json['owner_id'])
    subject = Subject.query.get(request.json['subject_id'])

    grade.value = value
    grade.owner = owner
    grade.subject = subject

    db.session.commit()
    return pupil_schema.jsonify(grade)

# endpoint to update subject
@app.route("/subject/<id>", methods=["PUT"])
def subject_update(id):
    subject = Subject.query.get(id)
    name = request.json['name']

    subject.name = name

    db.session.commit()
    return subject_schema.jsonify(subject)


# endpoint to show pupil
@app.route("/pupil/<id>", methods=["GET"])
def pupil_get(id):
    pupil = Pupil.query.get(id)
    return pupil_schema.jsonify(pupil)


# endpoint to show pupil
@app.route("/avg_grade/<user_id>/<subject_id>", methods=["GET"])
def avg_grade(user_id, subject_id):
    pupil = Pupil.query.get(user_id)
    avg = pupil.avg(subject_id)
    return jsonify({'result': avg})



