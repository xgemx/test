import json
from flask import Flask
import os

app = Flask(__name__)
basedir = os.path.abspath(os.path.dirname(__file__))
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'journal.sqlite')

class CustomJSONEncoder(json.JSONEncoder):
    def default(self, o):
        if hasattr(o, '_to_json'):
            return o._to_json()
        return json.JSONEncoder.default(self, o)

app.json_encoder = CustomJSONEncoder
from views import *
if __name__ == '__main__':
    app.run(host='0.0.0.0')
