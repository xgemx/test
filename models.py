from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import relationship
from functools import reduce
from flask_migrate import Migrate
from app import app


db = SQLAlchemy(app)
migrate = Migrate(app, db)

# Модель реализует сущность ученика
class Pupil(db.Model):
    __tablename__ = 'pupil'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=True)
    grades = relationship(
        "Grade",
        backref="owner",
        lazy='dynamic'
    )

    def avg(self, subject_id=None):
        if subject_id:
            l = self.grades.filter(Grade.subject_id == subject_id)
        else:
            l = self.grades
        if l.count() != 0:
            return reduce(lambda res, x: res + x.value, l, 0) / l.count()
        else:
            return 0

    def _to_json(self):
        return {
            "id": self.id,
            "name": self.name
        }

# Модель реализует сущность оценки
class Grade(db.Model):
    __tablename__ = 'grade'
    id = db.Column(db.Integer, primary_key=True)
    value = db.Column(db.Integer, nullable=False)
    owner_id = db.Column(db.Integer, db.ForeignKey('pupil.id'))
    subject_id = db.Column(db.Integer, db.ForeignKey('subject.id'))

    def _to_json(self):
        return {
            "id": self.id,
            "value": self.value,
            "owner": self.owner,
            "subject": self.subject
        }

# Модель реализует сущность школьного предмета
class Subject(db.Model):
    __tablename__ = 'subject'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=True)
    grades = relationship("Grade", backref="subject")

    def _to_json(self):
        return {
            "id": self.id,
            "name": self.name
        }