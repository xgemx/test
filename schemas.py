from flask_marshmallow import Marshmallow
from app import app

ma = Marshmallow(app)

class PupilSchema(ma.Schema):
    class Meta:
        fields = ('id', 'name', 'avg')


class GradeSchema(ma.Schema):
    class Meta:
        fields = ('id', 'value', 'owner', 'subject')

class SubjectSchema(ma.Schema):
    class Meta:
        fields = ('id', 'name', 'grades')

pupil_schema = PupilSchema()
pupils_schema = PupilSchema(many=True)

grade_schema = GradeSchema()
grades_schema = GradeSchema(many=True)

subject_schema = SubjectSchema()
subjects_schema = SubjectSchema(many=True)